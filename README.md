# Installation

```bash
mkdir -p ~/bin
echo "PATH=$PATH:~/bin" >> .zshrc
curl https://gitlab.com/kurtbomya/optimization-scripts/-/raw/main/gitcm -o ~/bin/gitcm
chmod 770 ~/bin/gitcm
curl https://gitlab.com/kurtbomya/optimization-scripts/-/raw/main/branchme -o ~/bin/branchme
chmod 770 ~/bin/branchme
```

# gitcm

Quickly make git commits and give a visual queue that you can cancel out of in case you panic and don't want to push!

Sometimes when you're working in an IDE, your terminal may be at a different position than the root of the repository. This script finds root (assuming you store your code in `~/code`, update `CODE_DIR` if not) and commits everything from the top down.

### Usage
Whenever you're done working, write `gitcm "commit message"` in place of `git add .`, `git commit -m "commit message"` and `git push`.

# branchme

Quickly make git branches that follow the convention `feature/username-iso8601date-reason-for-branch` and give a visual queue that you can cancel out of in case you panic and don't want to use that name.

Sometimes when you're creating lots of branches, it's hard to consistently nail the required names required by an upstream hook. This offloads that cognative load.

### Usage
Whenever you're needing a new branch, use syntax like `branchme "remove bacon from machine"` and this will be converted to `feature/kurtbomya-2023-04-25-remove-bacon-from-machine` (lowercased and dash-delimited)

### Warnings
- `branchme` will not work unless your primary branch is named `main`.
