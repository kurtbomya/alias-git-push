#!/bin/bash

# Set colors
# Output Coloring
RD="\033[0;31m" # RED
GN="\033[0;32m" # GREEN
YW="\033[0;33m" # YELLOW
NC="\033[0m"    # RETURN TO NO COLOR
GREEN_CHECK='\u2705\uFE0F'

# Check if the first positional argument is given
if [ -z "$1" ]; then
  echo -e "${RD}ERROR${NC} - Supply branch purpose"
  exit 1
fi

# Check if the current directory is inside a Git repository
if ! git rev-parse --is-inside-work-tree >/dev/null 2>&1; then
  echo -e "${RD}ERROR${NC} - Not a git repository"
  exit 1
fi

# Get the iso8601 date format
iso8601_date=$(date +%Y-%m-%d)

# Get the current branch name
branch=$(git branch --show-current)

echo -e "${YW}CURRENT BRANCH${NC}: ${branch}"

if echo "$branch" | grep -q "main"; then
  main_set=1 # "main" was found in the branch name
else
  main_set=0 # "main" was NOT found in the branch name
  echo -e "${YW}WARNING${NC}: Setting to branch 'main', pulling and starting new branch creation"
fi

# dash delimieted
branch_reason=$(echo $1 | tr ' ' '-')
# lowercase
branch_reason=$(echo $branch_reason | tr '[:upper:]' '[:lower:]')
branch_name="feature/$(whoami)-${iso8601_date}-${branch_reason}"

echo -e "${GN}NOTE${NC}: New branch name will be: ${branch_name}"
### Wait some seconds for the user to panic and cancel:

# Get the width of the terminal
width=$(tput cols)

# Define the total number of iterations
total=25 # 2.5 = 2.5 seconds

# Loop through the iterations
for (( i=0; i<=$total; i++ ))
do
    # Calculate the number of filled and empty slots in the progress bar
    filled=$((i * width / total))
    empty=$((width - filled))

    # Create the progress bar string
    bar=""
    for (( j=0; j<$filled; j++ ))
    do
        bar+="="
    done
    for (( j=0; j<$empty; j++ ))
    do
        bar+=" "
    done

    # Print the progress bar
    printf "\r%s" "$bar"

    # Wait for 0.1 seconds before the next iteration
    sleep 0.1
done

# Print a newline character to move the cursor to the next line
printf "\n"

if [ $main_set -eq 1 ]; then
  # Already set to main
  git pull
else
  # Not set to main. Need to switch to main
  git checkout main
  git pull
fi

git checkout -b ${branch_name}



