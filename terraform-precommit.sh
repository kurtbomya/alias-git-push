#!/bin/sh
##################################################################
### PRE COMMIT HOOK FOR MAKING SURE FILE END WITH NEWLINE CHAR ###
##################################################################

# Get the list of staged files
staged_files=$(git diff --cached --name-only --diff-filter=d)

# Add a newline to the end of each staged file if needed
for file in $staged_files; do
    # Check if the file ends with a newline
    if [ "$(tail -c1 "$file" | wc -l)" -eq 0 ]; then
        # Add a newline to the end of the file
        echo >> "$file"

        # Stage the updated file
        git add "$file"
    fi
done

#############################################
### TERRAFORM AUTOMATED FMT BEFORE COMMIT ###
#############################################

# Check if there are any *.tf files in the commit
if ! git diff --quiet --cached -- '*.tf'; then
  echo "Terraform files found to be modified in this commit"

  # Check if terraform is installed locally on this machine
  if command -v terraform >/dev/null 2>&1 ; then
    echo "Terraform binary found on the PATH for this machine"

    echo "Running Terraform fmt now for the current repository"
    cd "$(git rev-parse --show-toplevel)" || return

    # Save the list of staged Terraform files before running terraform fmt
    before_fmt=$(git diff --cached --name-only '*.tf')

    # Format only the staged Terraform files
    for file in $before_fmt; do
      terraform fmt "$file"
      git add "$file"
    done

    # Save the list of staged Terraform files after running terraform fmt
    after_fmt=$(git diff --cached --name-only '*.tf')

    # Check if terraform fmt changed any files
    if [ "$before_fmt" != "$after_fmt" ]; then
      echo "ERROR: git pre-commit hook detects changes occurred. You will need to git add, and git commit again now."
      # Cancel out of the commit, turning control back over to the user.
      exit 1
    fi
  else
    echo "WARN: Terraform not found but tf files were, The pre-commit hook could not terraform fmt files."
  fi
  # No Terraform files in commit. Do nothing, say nothing.
fi

# Continue with the commit
exit 0
